import * as React from "react";
import { useDispatch } from "react-redux"; // useSelector
import { cartList } from "../Api/Api";
const Child = () => {
  // const stData = useSelector(state => state);
  const dispatch = useDispatch();

  // console.log("stData ---->", stData)

  const getTodo = () => {
    cartList().then(cartApi => {
      const cartData = cartApi.data.carts
      dispatch({ type: "All", data: JSON.stringify(cartData) })
    })
    
  }

  return (
    <div>
      {/* {stData.childreducer.cdata} */}
      <button onClick={(e) => getTodo()}>Cart List</button>
    </div>
  );
}

export default Child;
