import axios from "axios"
const host = "https://dummyjson.com/"
export const cartList = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${host + 'carts'}`).then((response) => {
            resolve(response);
        }).catch((failResponse) => {
            reject(failResponse);
        });
    });
}