
const initialization = {};
const childreducer = (state = initialization, action) => {
  // console.log("child reducer --->", state)
  // console.log("child reducer action --->", action)
  switch (action.type) {
    case "All":
      return {
        cdata: action.data ? action.data : "No Data"
      };
    default:
      return state;
  }
}

export default childreducer
