import * as React from "react";
import "./styles.css";
import { useSelector, useDispatch } from "react-redux";
import Child from "./compoents/Child";
import ShowTodo from "./compoents/ShowTodo";

export default function App() {
  const counter = useSelector(state => state);
  // console.log("counter --->", counter);
  const dispatch = useDispatch();
  // const uMemo = useMemo(() => {
  //   return <Child />;
  // }, []);
  return (
    <div className="App">
       <h1>{counter.reducer.vehicle}</h1>
      <button
        onClick={() =>
          dispatch({
            type: "Car"
          })
        }
      >
        Car
      </button>
      <button
        onClick={() =>
          dispatch({
            type: "Bike"
          })
        }
      >
        Bike
      </button>
      {/* {uMemo} */}
      <Child />
      <ShowTodo />
    </div>
  );
}
